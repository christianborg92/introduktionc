// Laboration_2_del_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>

using namespace std;

#include <iostream>
#include <string>
using namespace std;

//en method som kontrollerar indatat
bool onlyDigits(string str)
{

	//variablar
	int num = stoi(str);
	int start = num;
	int digit;
	int reversed = 0; 
	string str_numb = to_string(num);//endast för att sedan kontrollera ifall det är 5 siffror(gjorde nån typ av felhantering)

	//kolla ifall det är 5 siffror med
	if (str_numb.length() != 5)
	{
		return false;
	}

	//gör detta tills num inte är 0
	do
	{
		digit = num % 10;
		reversed = (reversed * 10) + digit;
		num = num / 10;
	} while (num != 0);

	//Skriv ut talet åt andra hållet 
	cout << "Talet bak och fram är : " << reversed << endl;

	//kolla ifall det du skrev in är samma sak som bak och fram.
	if (start == reversed)
	{
		return true; 
	}
	else
	{
		return false;
	}
}

int main()
{
	//variablar 
	string tal;
	int ea = 0;

	//gör detta så länge metoden som är skriven ovan inte returner true
	do
	{
		if (ea == 0)
		{
			cout << "Skriv in ett tal på 5 siffror: ";
		}
		else
		{
			cout << "Skrev du in ett tal på 5 siffror? Testa igen: ";
		}
		cin >> tal;
	} while (onlyDigits(tal) == false);

	cout << "Det är ett så kallat palindrome." << endl;

	system("pause");
	
}



