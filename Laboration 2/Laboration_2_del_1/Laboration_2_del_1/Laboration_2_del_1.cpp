// Laboration_2_del_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

int main()
{	
	//Greja variablar
	int a = 500;
	int b = 1;
	int c = sqrt((a * a) + (b * b)); // hypotenusan

	//Så länge hypotenusan är under 500 kör denna kod
	while (c <= 500 && a <= 500 && b <= 500)
	{
		//statement som säger att när hypotenusan är något av dessa värden kör denna kod
		if (c == 100 || c == 200 || c == 300 || c == 400 || c == 500)
		{
			//Utskrivning
			cout << "Katet A: "<< a << endl << "Katet B: "<< b << endl << "Hypotenusan: " << c << endl;
		}

		//öka värdet på variablarna
		a += 2; // Katet A 
		b += 5; // Katet B
		
		c = sqrt((a * a) + (b * b)); //Hyptenusan
	}

	system("pause");
}



