// Laboration_2_del_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h" 
#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
	unsigned char a;
	int counter = 31;

	//loopar igenom allt (börjar på 32)
	for (a = 32; a < 255; a++)
	{
		//lägg till värde i räknaren, så att vi får en lista
		counter++;
		//skriv ut värdena 
		cout << setw(5) << counter << "  " << a;
	}

	system("pause");
}

