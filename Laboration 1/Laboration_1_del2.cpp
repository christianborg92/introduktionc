
#include "stdafx.h"
#include <iostream>
using namespace std;

int main()
{
	// Variables and constants
	double radius, circumference, area; //jag gjorde om den till en double s� att �ven decimaler kommer med
	const double PI = 3.14; //Anv�nd punkt inst�llet f�r komma

	// Input the circles radius
	cout << "Assign the circle's radius: ";
	cin >> radius; //Anv�nd alligatormunnarna ist�llet f�r "=" annars tror programmet att vi f�rs�ker s�tta ett v�rde i en variable 

	// Algorithm to calculate circumference (2*PI*r) and area (PI*r*r)
	circumference = PI * radius * radius;
	area = 2 * PI * radius; //variabeln PI �r i stora bokst�ver. Samt s� kombineras int med float...(bytte allt mot double(enkel l�sning)

	// Output of results
	cout << "A circle with the radius " << radius << " has the " << circumference << " circumference and " << area << " area " << endl;

	// Validate x
	int x;
	cin >> x;

	if (x == 100) // Det ska vara dubbla likamed tecken "=="
		cout << "x is equal to 100" << endl;
	if (x > 0) // tagit bort en bracket
		cout << "x is larger than zero" << endl;

	switch (x) {
	case 5: cout << "x is equal to 5 " << endl;
	case 10: cout << "x is equal to 10" << endl;
	default: cout << "x is neither 5 nor 10" << endl;

		return 0;
	} // End main
}//Fanns ingen bracket i slutet. 