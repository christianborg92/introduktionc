// Laboration.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

int main()
{
	/*Här sätter vi alla variablar*/
	int start;
	int end; 
	float amount;
	const double gasprice = 15.66; 

    /*Här startar vi lite utmaning kring frågor & sätter in lite värden i våra variablar*/
	cout << "Ange mätarställningen 1 [km]: ";
	cin >> start;
	cout << "Ange mätarstälnningen 2 [km]: ";
	cin >> end; 
	cout << "Hur många liter tankade du? ";
	cin >> amount;

	/*Gör lite uträkningar */
	int km = (end - start) / 10;
	double divide = amount / km;
	double price = divide * gasprice;

	/*Matar ut lite information i konsolen*/
	printf("\r\nBensinförbrukning [L/mil] : %.2f\n", divide);
	printf("Milkostnad        [kr/mil] : %.2f\n", price);
	
	/*Har den bara för att konsolen ska stanna upp*/
	string hejsan;
	cin >> hejsan;

}

